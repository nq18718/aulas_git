# Nome do Sistema

* New Shoe Store

# Função/Objetivo do Sistema

* Criar, alterar e apagar produtos bem como ordenar ordem crescente e decrescente

# Ambiente de Desenvolvimento 

## linguagem de programação


* JAVA


## IDE


* Eclipse


## versão de software 


* versão actual - v4.14
	

# Tutorial para correr o Sistema

## *Docker:*

#### Build 
docker build -t projectojava:v1.0 .
#### Run
docker run -it projectojava:v1.0

## *Linux:*
	
* correr o comando sudo apt install default-jre
* entrar na pasta:
	* cd projecto/Javaapplication1/src/javaapplication1
* efectuar o comando seguinte:
	* java Javaapplication1.java
	
## *Windows:*

* instalar git bash e utilizar informação de linux.

# Como utilizar o Sistema
	
Ao inserir o número:

1. Inserir um sapato -> onde vai ser pedido a introdução de um modelo, quantidade e o preço de um sapato.

2. Eliminar um sapato -> onde vai ser pedido o id do sapato que quer eliminar.

3. Demostração lista de sapatos -> onde vai ser demostrado a lista de sapatos existentes.

4. Edição de um determinado sapato -> onde vai ser pedido o numero de id do sapato a ser editado e repetido o ponto 1.

5. Demostração de lista de sapatos por ordem crescente -> onde vai ser demostrado a lista de sapatos existentes por ordem crescente pelo modelo de sapato.

6. Demostração de lista de sapatos por ordem decrescente -> onde vai ser demostrado a lista de sapatos existentes por ordem decrescente pelo modelo de sapato.

7. Sair do programa -> onde vai poder finalizar a execução do programa.

# Membros da Equipa e sua respectiva responsabilidade

* **Rui Gomes** - Scrum Master/ Developer
* **Rúben Antunes** - Developer
* **Ricardo Pereira** - Developer
* **Nuno Queiroz** - Devoloper

# Funcionalidades Implementadas


* **Criação de artigos**
	* id automático
	* Modelo do artigo
	* Quantidade
	* Preço
* **Alteração do artigo por id**
* **Eliminação do artigo por id**
* **Listar artigo por id**
* **Ordenar artigo alfabeticamente - ordem crescente**
* **Ordenar artigo alfabeticamente - ordem decrescente**
	
# Funcionalidades Futuras

* **Adicionar tamanho do sapato na sua criação**
* **Correção da estrutura do menu**
* **Adicionar género do sapato**
* **Fazer limpeza de código**
